<!DOCTYPE html>
<html>
  <head>
    <title>Sample "Hello, World" Application!</title>
  </head>
  <body bgcolor=blue>
	<h1>Sample "Hello, World" Application</h1>

    <p>This is the home page for the HelloWorld Web application. </p>
    <p>To prove that they work, you can execute either of the following links:
    <ul>
      <li>To a JSP page: <a href="hello.jsp">hello.jsp</a>.
      <li>To a servlet: <a href="/hello">hello</a>.
    </ul>

  </body>
</html>